import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { AuthService } from '../shared/auth/auth.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit {

  topLinks = [
    {title: 'Annonces', url: '/feed', icon: 'list'},
    {title: 'Mes enchères', url: '/bid', icon: 'pricetag'},
    {title: 'Ma boutique', url: '/shop', icon: 'wallet'}
  ];

  bottomLinks = [
    {title: 'Mon compte', url: '/account', icon: 'person'}
  ];

  constructor(private menuCtrl: MenuController,
              private authService: AuthService) { }

  ngOnInit() {
    this.menuCtrl.close();
  }

  logout() {
    this.authService.logout();
  }

}
