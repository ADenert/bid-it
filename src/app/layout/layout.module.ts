import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout.component';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { RouteReuseStrategy, RouterModule } from '@angular/router';



@NgModule({
  declarations: [LayoutComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    CommonModule,
    RouterModule
  ], 
  providers: [{provide: RouteReuseStrategy, useClass: IonicRouteStrategy}],
  exports: [LayoutComponent]
})
export class LayoutModule { }
