import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  resourceUrl = "https://europe-west1-bids-app-f9013.cloudfunctions.net/api"

  constructor(private http: HttpClient) { }

  findAll() {
    return this.http.get(`${this.resourceUrl}/user`);
  }

  find(uid) {
    return this.http.get(`${this.resourceUrl}/user/${uid}`);
  }

}
