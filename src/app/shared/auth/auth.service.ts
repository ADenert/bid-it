import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  resourceUrl = "https://europe-west1-bid-app-44f72.cloudfunctions.net/api"

  constructor(private http: HttpClient,
              private router: Router) { }

  getToken(): string {
    return localStorage.getItem('token');
  }

  isAuthenticated(): boolean {
    const token = this.getToken();
    return token ? true : false;
  }

  register(newUser): Observable<any> {
    return this.http.post(`${this.resourceUrl}/register`, newUser)
    .pipe(
      tap(res => {
      if(res['token']) {
        localStorage.setItem('token', res['token']);
        this.router.navigate(['/feed']);
      }}));
  }

  login(loginDatas) {
    return this.http.post(`${this.resourceUrl}/login`, loginDatas)
    .pipe(
      tap(res => {
      if(res['token']) {
        localStorage.setItem('token', res['token']);
        this.router.navigate(['/feed']);
      }}));
  }

  getLoggedUser() {
    return this.http.get(`${this.resourceUrl}/me`)
  }

  logout() {
    localStorage.setItem('token', null);
    this.router.navigate(['/login']);
  }


}
