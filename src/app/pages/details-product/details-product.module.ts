import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailsProductPageRoutingModule } from './details-product-routing.module';

import { DetailsProductPage } from './details-product.page';
import { ProductDetailsModule } from 'src/app/common/product/product-details/product-details.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailsProductPageRoutingModule,
    ProductDetailsModule
  ],
  declarations: [DetailsProductPage]
})
export class DetailsProductPageModule {}
