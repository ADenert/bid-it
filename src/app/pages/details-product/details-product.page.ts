import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/common/product/shared/product.service';
import { ActivatedRoute } from '@angular/router';
import { NavParams } from '@ionic/angular';

@Component({
  selector: 'app-details-product',
  templateUrl: './details-product.page.html',
  styleUrls: ['./details-product.page.scss'],
})
export class DetailsProductPage implements OnInit {

  product$: any;

  constructor(private productService: ProductService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.product$ = this.productService.find(this.route.snapshot.params.id);
  }

}
