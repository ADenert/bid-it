import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ProductService } from 'src/app/common/product/shared/product.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.page.html',
  styleUrls: ['./shop.page.scss'],
})
export class ShopPage implements OnInit {

  products$: Observable<any[]>

  constructor(private router: Router,
               private productService: ProductService) { }

  ngOnInit() {
    this.products$ = this.productService.findByUser();
  }

  goToAdd() {
    this.router.navigate(['/new-product'])
  }

}
