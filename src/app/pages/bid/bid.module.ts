import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BidPageRoutingModule } from './bid-routing.module';

import { BidPage } from './bid.page';
import { ProductListModule } from 'src/app/common/product/product-list/product-list.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BidPageRoutingModule,
    ProductListModule
  ],
  declarations: [BidPage]
})
export class BidPageModule {}
