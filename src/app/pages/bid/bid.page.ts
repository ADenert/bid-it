import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductService } from 'src/app/common/product/shared/product.service';

@Component({
  selector: 'app-bid',
  templateUrl: './bid.page.html',
  styleUrls: ['./bid.page.scss'],
})
export class BidPage implements OnInit {

  products$: Observable<any[]>

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.products$ = this.productService.findByUserParticipation();
  }

}
