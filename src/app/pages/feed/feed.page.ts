import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { ProductService } from 'src/app/common/product/shared/product.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.page.html',
  styleUrls: ['./feed.page.scss'],
})
export class FeedPage implements OnInit {

  products$: Observable<any[]>

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.products$ = this.productService.findAll();
  }

}
