import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/shared/auth/auth.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

  loggedUser$: Observable<any>

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.loggedUser$ = this.authService.getLoggedUser();
  }

}
