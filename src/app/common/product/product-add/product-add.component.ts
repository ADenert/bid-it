import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ProductService } from '../shared/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.scss'],
})
export class ProductAddComponent implements OnInit {

  productForm = this.fb.group({
    title: [''],
    categories: [''],
    duration: [''],
    startPrice: ['']
  });

  categories = ['Véhicule', 'Meuble', 'Objet de collection', 'Éléctroménagé']

  constructor(private fb: FormBuilder,
              private productService: ProductService,
              private router: Router) { }

  ngOnInit() {}

  addProduct() {
    this.productService.create(this.productForm.value).subscribe(() => this.router.navigate(['/shop']));
  }

}
