import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  resourceUrl = "https://europe-west1-bid-app-44f72.cloudfunctions.net/api";

  constructor(private http: HttpClient) { }

  findAll(): Observable<any> {
    return this.http.get(`${this.resourceUrl}/product`);
  }

  findByUser(): Observable<any> {
    return this.http.get(`${this.resourceUrl}/me/on-sale`);
  }

  findByUserParticipation(): Observable<any> {
    return this.http.get(`${this.resourceUrl}/me/on-bid`);
  }

  find(productId): Observable<any> {
    return this.http.get(`${this.resourceUrl}/product/${productId}`);
  }

  create(product): Observable<any> {
    return this.http.post(`${this.resourceUrl}/product`, product);
  }

  remove(productId): Observable<any> {
    return this.http.delete(`${this.resourceUrl}/product/${productId}`);
  }

  bid(productId, amount) {
    return this.http.post(`${this.resourceUrl}/product/${productId}/bids`, {amount});
  }

}
