import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/shared/auth/auth.service';
import { ProductService } from '../shared/product.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
})
export class ProductDetailsComponent implements OnInit {

  @Input() product;
  ownerId$: Observable<boolean>
  bidForm = this.fb.group({
    amount: ['']
  });
  bidValue: number;

  constructor(private authService: AuthService,
              private productService: ProductService,
              private fb: FormBuilder,
              private router: Router) { }

  ngOnInit() {
    this.ownerId$ = this.authService.getLoggedUser().pipe(map(user => user['id']));
    this.bidValue = 0;
  }

  bid() {
    this.productService.bid(this.product.id, this.bidForm.get('amount').value).subscribe();
  }

  bidDown() {
    if (this.bidValue > this.getCurrentPrice()) {
      this.bidValue -= 1;
    }
  }

  bidUp() {
    if (this.bidValue > this.getCurrentPrice()) {
      this.bidValue += 1;
    } else {
      this.bidValue = this.getCurrentPrice() +1;
    }
  }

  getCurrentPrice(): number {
    if(this.product?.bids.length) {
      return this.product.bids.sort((a, b) => b.amount - a.amount)[0].amount;
    } else {
      return this.product?.startPrice;
    }
  }

  isBidDisable(uid) {
    if (this.product.bids.length) {
      return this.product.bids.sort((a, b) => b.amount - a.amount)[0].userId === uid
        ? true
        : false;
    } else {
      return false;
    }
    
  }

  deleteProduct() {
    this.productService.remove(this.product.id).subscribe(res => {
      this.router.navigate(['/shop'])
    })
  }

}
