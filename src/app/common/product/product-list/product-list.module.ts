import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductListComponent } from './product-list.component';
import { IonicModule } from '@ionic/angular';
import { ProductListItemComponent } from './product-list-item/product-list-item.component';



@NgModule({
  declarations: [ProductListComponent, ProductListItemComponent],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports:[ProductListComponent]
})
export class ProductListModule { }
