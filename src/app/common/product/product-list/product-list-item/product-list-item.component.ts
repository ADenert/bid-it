import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-list-item',
  templateUrl: './product-list-item.component.html',
  styleUrls: ['./product-list-item.component.scss'],
})
export class ProductListItemComponent implements OnInit {

  @Input() product: any;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToDetails() {
    this.router.navigate(['/details', this.product.id])
  }

}
