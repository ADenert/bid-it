import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'feed',
    pathMatch: 'full'
  },
  {
    path: '',
    component: LayoutComponent,
    // Use guards here
    children: [
      {
        path: 'feed',
        loadChildren: () => import('./pages/feed/feed.module').then( module => module.FeedPageModule)
      },
      {
        path: 'bid',
        loadChildren: () => import('./pages/bid/bid.module').then( module => module.BidPageModule)
      },
      {
        path: 'shop',
        loadChildren: () => import('./pages/shop/shop.module').then( module => module.ShopPageModule)
      },
      {
        path: 'account',
        loadChildren: () => import('./pages/account/account.module').then( module => module.AccountPageModule)
      },
      {
        path: 'new-product',
        loadChildren: () => import('./pages/add-product/add-product.module').then( m => m.AddProductPageModule)
      },
      {
        path: 'details/:id',
        loadChildren: () => import('./pages/details-product/details-product.module').then( m => m.DetailsProductPageModule)
      }
    ]
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( module => module.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( module => module.RegisterPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
